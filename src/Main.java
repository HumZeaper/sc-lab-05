public class Main {

	public static void main(String[] args) {
		Library l = new Library();
		Student s = new Student("Nutcharapon Siriphitukyotin","5610450977","D14");
		Teacher t = new Teacher("Chakon Poungsereekul","D14") ;
		Peopleware people = new Peopleware("HumZeaper","Security Guard") ;
		
		
		Book book1 = new Book("Software Construction","2014");
		Book book2 = new Book("Wikipedia","2015");
		Book book3 = new Book("Computer Programming","2011");
		Book book4 = new Book("Software Analysis and Design","2009");
		Book book5 = new Book("Data Structure","2013");
		Book book6 = new Book("Database","2009");
		ReferenceBook refBook = new ReferenceBook("Java Reference","2009");
		ReferenceBook refBook2 = new ReferenceBook("wikipedia","2015");
		ReferenceBook refBook3= new ReferenceBook("Computer Programming for newbie","2011");
		ReferenceBook refBook4 = new ReferenceBook("MySQL","2002");

		
		l.addBook("Software Construction",book1);
		l.addBook("Wikipedia",book2);
		l.addRefBook("Java Reference",refBook);
		System.out.println(l.getBookCount()); 
		l.borrowBook(s,"Wikipedia");  
		System.out.println(l.getBookCount()); 
		l.returnBook(s,"Wikipedia"); 
		System.out.println(l.getBookCount()); 
		l.borrowBook(s,"Java Reference");
		System.out.println(l.getBookCount()); 
		System.out.println(s.getName()); 

	}
}
